import json
import pytest

from django.core.urlresolvers import reverse
from django.test import Client
from django_scim.models import SCIMUser

from .test_utils import _make_django_user


@pytest.mark.django_db
def test_user_view():
    """Test getting a user by id via SCIM"""
    c = Client()
    user = _make_django_user()
    scim_user = SCIMUser(user)
    url = reverse('scim-user', args=[user.pk])
    response = c.get(url)

    assert scim_user.to_dict() == json.loads(response.content)


@pytest.mark.django_db
def test_search_view():
    """Look a user up by username using SCIM search filter"""
    c = Client()
    user = _make_django_user()

    url = reverse('scim-search')
    data = json.dumps(
        {'filter': 'username eq "{}"'.format(user.username)}
    )

    response = c.post(url,
                      data=data,
                      content_type='application/json')

    res_data = json.loads(response.content)['Resources'][0]
    assert res_data['userName'] == user.username
    assert res_data['emails'] == [{u'primary': True,
                                   u'value': user.email}]


@pytest.mark.django_db
@pytest.mark.parametrize('hasher', [
                         'bcrypt_sha256',
                         'bcrypt',
                         'sha1',
                         'shaI',  # See hashers.OldSHA1PasswordHasher
                         ])
def test_search_view_password(hasher):
    """Look a user up by password using SCIM search filter"""
    c = Client()
    user = _make_django_user(hasher)

    url = reverse('scim-search')
    data = json.dumps(
        {'filter': 'password eq "biscuits"'}
    )

    response = c.post(url,
                      data=data,
                      content_type='application/json')

    res_data = json.loads(response.content)['Resources'][0]
    assert res_data['userName'] == user.username
    assert res_data['emails'] == [{u'primary': True,
                                   u'value': user.email}]


@pytest.mark.django_db
def test_search_escape_chars():
    name = r'my \ "name"'
    c = Client()
    user = _make_django_user(username='escaped', first_name=name)

    url = reverse('scim-search')
    data = json.dumps(
        {'filter': r'givenName eq "my \\ \"name\""'}
    )

    response = c.post(url,
                      data=data,
                      content_type='application/json')

    res_data = json.loads(response.content)['Resources'][0]
    assert res_data['userName'] == user.username
    assert res_data['name']['givenName'] == name
